# What is this?

Get perfect shadows every time for the non-designer.

# Installation

`npm i fy-shadowizard --save`

Then...

```
import { shadowizard } from 'fy-shadowizard';

shadowizard({
    shadow_type: 'soft',
    padding: false
});
```

## Options

Shadowizard supports 2 options, both of which are optional:

* *show_type* - _hard | soft_ (Defaults to soft)
* *padding* - _boolean_ (Defaults to false)